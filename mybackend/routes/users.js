const express = require('express')
const router = express.Router()
const userController = require('../controller/UserController')
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json(userController.getUser())
})

router.get('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(userController.getUserID(id))
})

router.post('/', function (req, res, next) {
  const payload = req.body
  res.json(userController.addUser(payload))
})

router.put('/:id', function (req, res, next) {
  const payload = req.body
  res.json(userController.updateUser(payload))
})

router.delete('/:id', function (req, res, next) {
  const { id } = req.body
  res.json(userController.deleteUser(id))
})

module.exports = router
