const userController = {
  userList: [
    { id: 1, name: 'Satarat', gender: 'M' },
    { id: 2, name: 'Satarat2', gender: 'M2' }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
    return user
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { index }
  },
  getUser () {
    return [...this.userList]
  },
  getUserID (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}
module.exports = userController
