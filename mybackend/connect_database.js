
const mongoose = require('mongoose')
const User = require('./moduls/User')

mongoose.connect('mongodb://localhost/db', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection

db.on('error', console.error.bind(console, 'Connection Error : '))
db.once('open', function () {
  console.log('connection Database')
})

// eslint-disable-next-line array-callback-return
User.find(function (err, users) {
  if (err) {
    return console.error(err)
  }
  console.log(users)
})
